<!doctype html>
<!-- view.php

HMM Viewer, a new way of visualizing profile Hidden Markov Models.
Copyright (C) 2013 Jens Rauch and Bianca Regenbogen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses. -->
<html>
	<head>
		<meta charset="utf-8" />
		<title>HMM Viewer (Beta)</title>
		<!-- view.css -> layout hmm viewer, main_site.css -> layout website -->
		<link rel="stylesheet" href="./css/view.css">
		<link rel="stylesheet" href="./css/main_site.css">
		<script src="./js/Detector.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/three.min-r63.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/Stats.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/TrackballControls.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/jquery-2.0.3.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/Hmmview.js" type="text/javascript" charset="UTF-8"></script>
		<link type="text/css" href="./css/custom-theme/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
		<script type="text/javascript" src="./js/jquery-ui-1.10.3.custom.min.js"></script>
	</head>
	<script language="javascript" type="text/javascript">
	//log upload process
	function uploadProgress(output){
		$('input.fileb').attr('disabled', 'disabled');
		$(output).appendTo('div.log');
		$("div.log").scrollTop($("div.log").get(0).scrollHeight);
	}
	</script>
	<body>
		<div id="mainb">
			<div id="header">
				<span class="htext"><a href="./index.php" class="htext"><font color="#F0F1F2">HMM</font>Viewer</a></span><span class="htextadd">Beta</span>
				<div class="headnav">
					<a class="mentryactive" id="home" href="./index.php"><span id="ihome">Home</a></span>
					<a class="mentry" id="mupload"><span id="iupload">Upload</a></span>
					<a class="mentry" id="mhelp" href="./help.pdf" target="_blank"><span id="ihelp">Help</a></span>
					
				</div>
				<div>
					<br /><span class="bookmark">> <b>Bookmark</b> for direct access</span>
				</div>
			</div>
			<div id="mainsite">
				<?php include("./about.html"); ?>
				<div class="tooltip"></div>
				<div class="delfile" title="Confirm"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Do you really want to delete this file?</p></div>
				<div class="select">
					<div id="headw"><a href="./help.pdf#page=x" target="_blank"><div id="selectinfo" class="infopic"></div></a><div class="headtext">Select HMM</div></div>
					<div id="inner">
						<p><b>Browse the list of available HMMs</b></p>
						<form action="./view.php" method="get">
						<?php
							$thisdir = getcwd()."/hmms/";
							$handle = opendir("$thisdir");
							$ic=0;
							while (false !== ($file = readdir($handle))) {
								if($file!="." AND $file!=".."){
									$ic++;
								}
							}
							closedir($handle);
							$in=0;
							$handle = opendir("./hmms");
							if($ic>=20){ $ic=20; }
							echo "<select name='id' id='idchoose' class='choosehmm' size='".$ic."'>";
							while (false !== ($file = readdir($handle))) {
								if($file!="." AND $file!=".."){
									if($in % 2 == 0){
										echo "<option class='optndark' value='".$file."'>".$file."</option>";
									}
									else{
										echo "<option class='optdark' value='".$file."'>".$file."</option>";
									}
									$in++;
								}
							}
							echo "</select><br /><br />";
							closedir($handle);
						?>
						<input type="submit" class="ftpchooseb" value="Choose for visualization" disabled/><button type="button" class="delete" disabled/>Delete this file</button><br />
						</form>
						<br />
					</div>
				</div>
				<div class="select">
					<div id="headw"><a href="./help.pdf#page=2" target="_blank"><div id="importinfo" class="infopic"></div></a><div class="headtext">Import HMM</div></div>
					<div id="inner">
						<p><b>Import HMM directly from Pfam</b></p>
						Just browse on the Pfam website to the protein family of your liking and copy the link to the summary in the field below.<br /><br />
						<b>Example</b> if you want to view the family with the Pfam ID PF04617 you copy and past the URL:<br />
						<a href="http://pfam.sanger.ac.uk/family/PF04617">http://pfam.sanger.ac.uk/family/PF04617</a> in the field below:<br /><br />
						<form action="./view.php" method="get">
							<input name="id" id="idchoosepfam" size="80" type="text" placeholder="Put Pfam URL here"><br />
							<input type="submit" class="ftpchoosebpfam" value="Choose for visualization" disabled/><br />
						</form>
						<br />
					</div>
				</div>
				<div id="upload" class="stext">
					<div id="headw"><a href="./help.pdf#page=2" target="_blank"><div id="directupinfo" class="infopic"></div></a><div id="ctext" class="headtext">Upload</div></div>
					<div id="inner">
						<p class="noftptext"><b>Choose a HMM to upload</b></p>
						<form class="phpupform" action="./backend/upload.php" method="post" target="upload_target" enctype="multipart/form-data">
							<p id="uform">
								<input type="file" name="file" id="file" class="file" />
								<br /><br />
								<input type="submit" name="submit" value="Submit" class="fileb" disabled="disabled"/><br />
							</p>
							<iframe id="upload_target" name="upload_target" style="width:0;height:0;border:0px solid #fff;"></iframe>
						</form>
					</div>
				</div>
				<div id="log" class="stext">
					<div id="headw"><a href="./help.pdf#page=x" target="_blank"><div id="directupinfo" class="infopic"></div></a><div id="ctext" class="headtext">Log</div></div>
					<div id="inner">
						<div class="log"></div>
					</div>
				</div>
			</div>
			<div id="dialog-modal" title="Please wait!">
						<p>Your file is opening now.</p>
						<p>This could take up to a few minutes depending on the filesize and your computers processing power, please be patient.</p>
					</div>
			<div id="footer">
				<span class="fooleft"><span class="fooright">> About</span>
			</div>
		</div>
		<script>
			$(document).ready(function(){
				//TODO move all stuff possible to seperate javascript file
				//hide upload tab by default
				$("#upload").hide();
				$("#log").hide();
				//toggle upload/select view on menu click
				$('a#mupload').on('click', function(){
					$("#upload").show();
					$(".select").hide();
					$('a#mupload').attr('class', 'mentryactive');
					$('a#home').attr('class', 'mentry');
				});
				$('a#home').on('click', function(){
					$("#upload").hide();
					$(".select").show();
					$('a#home').attr('class', 'mentryactive');
					$('a#home').attr('class', 'mentry');
				});
				//bookmarkfunction
				$('.bookmark').on('click', function(){
					alert('Due to technical reasons please press ctrl+D to bookmark (Command+D for macs)');
				});
				//create about popup
				$( "#adialog" ).dialog({ autoOpen: false, width: 600, position: "top" });
				//show about when clicking on the link
				$( "span.fooright").on('click', function(){
					$("#adialog").dialog('open');
				});
				//activate/deactivate upload button on time
				$input2 = $('input.file');
				$input2.change(function(){
					var val2 = $input2.val();
					if(val2.length>0){ $('input.fileb').removeAttr('disabled'); }
					else{ $('input.fileb').attr('disabled', 'disabled'); }
				});
				$('input.fileb').on('click', function(){
					$("#log").show();
				});
				//init delete dialog
				$( ".delfile" ).dialog({ 
					autoOpen: false,
					modal: true,
					resizable: false,
					buttons: {
						Delete: function() {
							did = $("select option:selected").attr('value');
							var delresult = $.ajax({
								type: 'POST',
								async: false,
								url: './backend/delete.php',
								data: ({
									a: did
								})
							}).responseText;
							$(".delfile").html(delresult);
							$(".delfile").dialog( "option", "buttons", { "Ok": function() { document.location.reload(true); }} );
						},
						Cancel: function() {
							$( this ).dialog( "close" );
						}
					}
				});
				//activate choose button as soon as a file is selected
				if($("select#idchoose").val() != null){
					$('.ftpchooseb').removeAttr('disabled');
				}
				$("#idchoosepfam").bind('paste', function(e) {
	            	if($('.ftpchoosebpfam').attr('disabled')){
						$('.ftpchoosebpfam').removeAttr('disabled');
					}
				});
				$("#idchoosepfam").keyup(function(){
					if($("#idchoosepfam").val() != ""){
						$('.ftpchoosebpfam').removeAttr('disabled');
					}
					else {
						$('.ftpchoosebpfam').attr('disabled','disabled');
					}
				});
				$('select#idchoose').change(function(){
					$('.ftpchooseb').removeAttr('disabled');
					$('.delete').removeAttr('disabled');
					if($('#addfasta').val().length>0){
						$('.ftpchoosebseq').removeAttr('disabled');
						$('.ftpchooseb').attr('disabled','disabled');
					}
				});
				$('button.delete').on('click', function(){
					$('button.delete').removeClass("delete").addClass("confdelete");
					$('button.confdelete').on('click', function(){
						$(".delfile").dialog('open');
					});
				});
				//open loading dialog
				$( "#dialog-modal" ).dialog({
					height: 200,
					width: 500,
					modal: true,
					autoOpen: false
				});
				$('.ftpchooseb').on('click', function(){
					$( "#dialog-modal" ).dialog( "open" );
				});
				//initialize Tooltip
				var tooltip = $('<div class="tooltip"></div>').text('');
				$(tooltip).appendTo('body');
				//show tooltips
				$('#selectinfo').mouseenter(function(){
					tooltip.html('You can choose a HMM from the list below for visualization.<br> If no HMM is available upload a new one by selecting Upload from the top-right menu.<br>See manual chapter 2.1')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
				$('#importinfo').mouseenter(function(){
					tooltip.html('You can import a HMM from Pfam for visualization.<br\> Please follow the example below.<br\> See manual chapter 2.2')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
			});
		</script>
	</body>
</html>