<?php
// delete.php
// 
// HMM Viewer, a new way of visualizing profile Hidden Markov Models.
// Copyright (C) 2013 Jens Rauch and Bianca Regenbogen
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
$file = $_POST["a"];
$maindir = getcwd()."/../hmms/";
$dir=$maindir.$file;
unlink($dir);
foreach (scandir($dir) as $item) {
    if ($item == '.' || $item == '..') continue;
    unlink($dir.DIRECTORY_SEPARATOR.$item);
}
rmdir($dir);
echo "File successfully deleted!"
?>