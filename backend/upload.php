<?php
//set options for bett file upload
header('Content-Type: text/html; charset=utf-8');
header('X-Content-Type-Options: nosniff');
header('X-XSS-Protection: 1; mode=block');
set_time_limit(0);

//check if file was supplied
if ($_FILES["file"]["error"] > 0){
	echo "Error: " . $_FILES["file"]["error"] . "<br />";
	exit();
}
//Check for filename errors and correct them
$SafeFile = $_FILES["file"]["name"];
$SafeFile = str_replace("#", "No.", $SafeFile); 
$SafeFile = str_replace('$', "Dollar", $SafeFile); 
$SafeFile = str_replace("%", "Percent", $SafeFile); 
$SafeFile = str_replace("^", "", $SafeFile); 
$SafeFile = str_replace("&", "and", $SafeFile); 
$SafeFile = str_replace("*", "", $SafeFile); 
$SafeFile = str_replace("?", "", $SafeFile);

//set dir
$uploaddir = "../hmms/";

//check for valid ending, do not allow php files
$tmp=explode(".",$SafeFile);
$isphp=array_pop($tmp);
$phpending=array("php","php3","php4","php5");
if(in_array($isphp, $phpending)){
	unlink($_POST["passover"]);
	$result = "You have selected a .php file. This is due to security reasons not possible, please choose a valid HMM";
	echo '<script language="javascript" type="text/javascript">window.top.window.uploadProgress("<p>'.$result.'</p>");</script>';
	exit("PHP file upload is not allowed due to security reasons!");
}
unset($tmp);

$path = $uploaddir.$SafeFile;

while(file_exists($path)==TRUE){
	$result = "File already exists! Appending ID";
	echo '<script language="javascript" type="text/javascript">window.top.window.uploadProgress("<p>'.$result.'</p>");</script>';
	$id++;
	$SFile = $id.".".$SafeFile;
	$path = $uploaddir.$SFile;
}

if(empty($SafeFile)==FALSE){ //AS LONG AS A FILE WAS SELECTED... 
	if(move_uploaded_file($_FILES['file']['tmp_name'], $path)){ //IF IT HAS BEEN COPIED... 
	//GET FILE NAME 
	$theFileName = $_FILES['file']['name']; 
	//GET FILE SIZE 
	$theFileSize = $_FILES['file']['size']; 
	if ($theFileSize>999999999){ //IF GREATER THAN 999MB, DISPLAY AS GB 
		$theDiv = $theFileSize / 1000000000; 
		$theFileSize = round($theDiv, 1)." GB";
	}
	elseif ($theFileSize>999999){ //IF GREATER THAN 999KB, DISPLAY AS MB 
		$theDiv = $theFileSize / 1000000; 
		$theFileSize = round($theDiv, 1)." MB";
	}
	else { //OTHERWISE DISPLAY AS KB 
		$theDiv = $theFileSize / 1000; 
		$theFileSize = round($theDiv, 1)." KB";
	} 
	
	$result='<br>Original File Name: '.$theFileName.'<br>Saved as: '.$SFile.'<br>File Size: '.$theFileSize.'<br> Upload Successful!<br>';
	
	} else { 
		//PRINT AN ERROR IF THE FILE COULD NOT BE COPIED 
		$result = '<br><b>Error! File could not be uploaded.<br>Filename was '.$_FILES['file']['name'].'<br>Path was '.$path.'</b>';
	}
	echo '<script language="javascript" type="text/javascript">window.top.window.uploadProgress("<p>'.$result.'</p>");</script>';
}

?>
