\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}{section.0.1}
\contentsline {section}{\numberline {2}Methods}{2}{section.0.2}
\contentsline {subsection}{\numberline {2.1}Hidden Markov Models}{2}{subsection.0.2.1}
\contentsline {subsection}{\numberline {2.2}Motivation - Look on other tools}{2}{subsection.0.2.2}
\contentsline {section}{\numberline {3}Approach and Results}{3}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Overview}{3}{subsection.0.3.1}
\contentsline {subsection}{\numberline {3.2}Reading the data}{3}{subsection.0.3.2}
\contentsline {subsection}{\numberline {3.3}Display of the data}{3}{subsection.0.3.3}
\contentsline {subsubsection}{3D View}{3}{section*.2}
\contentsline {subsubsection}{Movement}{4}{section*.4}
\contentsline {subsubsection}{Coloring}{4}{section*.5}
\contentsline {subsubsection}{Settings}{5}{section*.6}
\contentsline {subsubsection}{Amino acid table}{5}{section*.7}
\contentsline {subsubsection}{Bookmarking}{6}{section*.8}
\contentsline {subsection}{\numberline {3.4}Index page}{6}{subsection.0.3.4}
\contentsline {subsubsection}{File-chooser}{6}{section*.9}
\contentsline {subsubsection}{Import from Pfam}{6}{section*.10}
\contentsline {subsubsection}{File upload}{6}{section*.11}
\contentsline {subsubsection}{Help page}{6}{section*.12}
\contentsline {section}{\numberline {4}Discussion}{6}{section.0.4}
\contentsline {subsection}{\numberline {4.1}Settings}{6}{subsection.0.4.1}
\contentsline {subsection}{\numberline {4.2}Bookmarking}{6}{subsection.0.4.2}
\contentsline {subsection}{\numberline {4.3}Save to file}{6}{subsection.0.4.3}
\contentsline {subsection}{\numberline {4.4}Evaluation - Comparing to other tools}{7}{subsection.0.4.4}
\contentsline {subsection}{\numberline {4.5}Outlook}{8}{subsection.0.4.5}
\contentsline {subsubsection}{Full-screen mode}{8}{section*.13}
\contentsline {subsubsection}{Save/restore camera}{8}{section*.14}
\contentsline {subsubsection}{Save as picture}{9}{section*.15}
\contentsline {subsubsection}{More color schemes}{9}{section*.16}
\contentsline {section}{\numberline {5}Supplementary}{9}{section.0.5}
