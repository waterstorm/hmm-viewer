\documentclass[10pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{float}
\restylefloat{table}
\usepackage{hyperref}
\usepackage{cite}
% Caption Settings:
\usepackage[hang]{caption}
\DeclareCaptionLabelFormat{bez}{#2}
\renewcommand{\thesection}{\arabic{section}}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{wrapfig}
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\small{Bioinformatics I Project - HMM Viewer}}
\fancyhead[R]{\small{Bianca Regenbogen and Jens Rauch}}
\fancyfoot[C]{Page \thepage\ of \pageref{LastPage}}

\begin{document}
\title{HMM Viewer}
\date{January 13, 2014}
\author{Bianca Regenbogen
        \and Jens Rauch}
        
\maketitle
\tableofcontents
\pagebreak
\section{Introduction}
During the use of Hidden Markov Models (HMMs) we discovered, that the current available programs to visualize these models are not quite what we expected. We wanted a tool which shows all important features and parameters of an HMM just by starting it up, without much navigation, such as clicking. Unfortunately, we could not find such a tool and therefore we decided to think about and implement a new way of visualizing HMMs.\\
The aim of this project was to resolve the big issues of the current tools, especially to see all important parameters at first sight. Our approach was the use of an interactive 3D web application to provide platform independence and ease of use. For navigation the mouse is used to analyze the model even further by showing all the small details and change perspective.

\section{Methods}

\subsection{Hidden Markov Models}
A Hidden Markov Model describes a more general version of a Markov Model. Therefore, the symbols of the alphabet are emitted by a visited state. The hidden Markov chain of states or the path sequence contains transition probabilities which depend on the previous state. The emission of the symbols is also referring to a certain emission probability.\\
So finally, to define a HMM following components are needed:
\begin{itemize}
 \item an alphabet of symbols representing the sequence
 \item a set of states to describe the path
 \item a transition matrix containing probabilities for the states
 \item an emission matrix containing probabilities for the symbols
 \end{itemize} 
There are several algorithms like Viterbi or the forward algorithm to decode the most probable path for a given sequence and known probabilities. For estimating the probabilities there are also different techniques according to the available values. The main similarity of all of them, for example using Maximum Likelihood, is learning the parameters. Because of the training, HMMs are a machine-learning approach.\\
The possibilities to apply HMMs are versatile. HMMs can be used in the field of describing DNA binding sites or motifs, predicting gene, protein or RNA structures and performing structural or multi-sequence alignments \cite{nieselt}. In the special case of Profile HMMs match, deletion and insert states are modeled. 

\subsection{Motivation - Look on other tools}
For visualizing data there are many techniques like classical displays within diagrams (e.g. histograms, pie charts, dot plots). Other general methods to visualize probabilities are color-maps or arc diagrams, especially RNAbows in the field of pair RNA alignments.\\ 
But for getting a view of a HMM there are only a few tools available. Most tools like the famous HMMLogo, which is a 2D-Visualization program provided by the web-service LogoMat-M, display the probabilities of transition and emission either by the height or width of symbols or lines\cite{hmmlogo}. Small probabilities are often difficult to read and all the information of the HMM is not displayed at first sight.\\ Another possibility to visualize a HMM is the classical graphical display of the insert and deletion states and their emissions. HMMEditor provides this function for profile HMMs \cite{hmmeditor}. But the user of this tool cannot see all possible symbols because of only visualizing the viterbi path.\\
To visualize HMMs in a new way, we wanted to display all information at first sight. To provide this function we determined the third dimension to be needed. Dimension one (positive) should represent all possible symbols emitted with their emission probability representing the match state and in the negative, the insert and deletion states. Dimension two refers to the complete chain of states and the corresponding sequence. In the third dimension the transition probabilities are visualized by the width or thickness.\\
Because of the famous HMMER application for searching homologies and getting information on sequence function, conservation and evolution of a generated profile HMM, our tool should process .hmm-files (HMMER 3), anyway \cite{hmmer}. Additionally, we decided to implement a function to support the Pfam web-base as well which provides a huge collection of HMMs in HMMER3 format \cite{pfam}.

\section{Approach and Results}
We used PHP \cite{php} to read in the files from the servers hard disk and JavaScript \cite{js} with the library THREE.JS \cite{threejs} for the three dimensional view and the framework jQuery \cite{jquery} for real-time interactions.

\subsection{Overview}
Figure 1 gives a short outline of how our tool works. Further details are available in the sections below.
\begin{center}
\includegraphics[scale=0.4]{overview.png}
\captionof{figure}{Overview of the steps performed by the web-server to create the website}
\end{center}
\subsection{Reading the data}
Currently our tool only supports HMMER3 formatted files, as it is one of the most used and a state of the art tool. For the read-in of the data we used PHP \cite{php}. It reads the complete file removes the header and splits all lines of the actual HMM and stores it in an array. This array is then transformed to a JSON object and directly given to JavaScript \cite{js} for further usage.

\subsection{Display of the data}
\subsubsection{3D View}
As mentioned above THREE.JS was used for the 3D visualization (figure 2).
\begin{center}
\includegraphics[scale=0.5]{3dview.png}
\captionof{figure}{The 3D view of the HMM Viewer (standard view and settings)}
\end{center}

\begin{wrapfigure}[21]{l}{4cm}
\includegraphics[scale=0.5]{popup.png}
\captionof{figure}{\footnotesize The pop-up with the detailed information}
\end{wrapfigure}
\bigskip
Each bar in the 3D view is subdivided in smaller cubes, each one representing one specific amino acid. The short name of each AA is printed directly on the cube (if the cube is big enough) and each amino acid has a specific color (see section coloring). The height of these bars represents the probability of the HMM staying in the match state.\\
The cylinders below the bars represent the insert- (blue) and delete states (gray). The thickness of the cylinders gets smaller, representing the probability of changing from a match state to an insert/delete state. For example, if the blue cylinder does not get smaller, the HMM has a probability of 100\% to change to the insert state at this position, while in contrary if it looks like a cone, the HMM has a probability of 0\% to change to the insert state.\\
The height of the cylinders shows the probability to stay in the actual state (analogue to the bars of the match state).\\
These detailed information are also presented in a detail pop-up (figure 3), which can be displayed by a simple double-click on a specific position.
\subsubsection{Movement}
The default movement for the HMM Viewer is panning the camera. This can be achieved by dragging while pressing down the left mouse button. As an extended feature you can also rotate the camera with the use of the right mouse button. You can reset the camera at any time using the "reset camera" button in the settings tab. It is currently not possible to zoom in or out. 
\subsubsection{Coloring}
According to the coloring of the 3D amino acid cubes, we wanted colors that are easy to differentiate and are comfortable to look at. To achieve this aim, Colorbrewer 2.0 was used which is an online tool for preparing color schemes sequential, diverging or qualitative \cite{brewer}.\\
\begin{center}
\begin{tabular}{llc}
AAs & group & color \\  
\hline
WLVIMFA & hydrophobic & \colorbox[HTML]{377EB8}{ } \\ 
KR & positively charged & \colorbox[HTML]{E41A1C}{ } \\ 
TSNQ & polar uncharged & \colorbox[HTML]{4DAA4A}{ } \\ 
DE & negatively charged & \colorbox[HTML]{984EA3}{ } \\ 
HY & aromatic & \colorbox[HTML]{A65628}{ } \\ 
C & Cysteine & \colorbox[HTML]{F781BF}{ } \\ 
G & Glycine & \colorbox[HTML]{FF7F00}{ } \\ 
P & Proline & \colorbox[HTML]{FFFF33}{ } \\ 
\hline
\end{tabular}  
\end{center}

\begin{wraptable}[20]{l}{4cm}
\begin{small}
\begin{tabular}{cc}
AA & color \\ 
\hline
W & \colorbox[HTML]{C6DBEF}{ } \\ 
L & \colorbox[HTML]{9ECAE1}{ } \\
V & \colorbox[HTML]{6BAED6}{ } \\ 
I & \colorbox[HTML]{4292C6}{ } \\ 
M & \colorbox[HTML]{2171B5}{ } \\ 
F & \colorbox[HTML]{08519C}{ } \\ 
A & \colorbox[HTML]{08306B}{ } \\ 
K & \colorbox[HTML]{FB6A4A}{ } \\ 
R & \colorbox[HTML]{CB181D}{ } \\ 
T & \colorbox[HTML]{A1D99B}{ } \\ 
S & \colorbox[HTML]{74C476}{ } \\ 
N & \colorbox[HTML]{31A354}{ } \\ 
Q & \colorbox[HTML]{006D2C}{ } \\ 
D & \colorbox[HTML]{C51B8A}{ } \\ 
E & \colorbox[HTML]{7A0177}{ } \\ 
H & \colorbox[HTML]{CC4C02}{ } \\ 
Y & \colorbox[HTML]{A65628}{ } \\ 
C & \colorbox[HTML]{F781BF}{ } \\ 
G & \colorbox[HTML]{FF7F00}{ } \\ 
P & \colorbox[HTML]{FFFF33}{ } \\ 
\hline
\end{tabular} 
\end{small}
\end{wraptable}
\bigskip 
\bigskip
For displaying the different amino acids contained in the HMM, two color schemes were created: One for coloring each amino acid by a single color and another one using one color for chemical similarities (special cases: C, G and P) of amino acids.\\
At first, the group colors were chosen similar to the colors used by ClustalX, JalView and Pfam \cite{colors} by applying Colorbrewer on 8 different qualitative colors. The corresponding colors are shown in the table above.\\
For the single colors, variants of the group color had been chosen by using Colorbrewer on the sequential setting. The table on the left displays the resulting colors which also indicate the chemical characteristics.\\
\\
As background color of the application a very light gray was picked for a smoother crossing from foreground and background.\\
The representation of the insert and deletion states are the cylinders below the amino acid cubes. In terms of marketing, we decided to fit the main advantage of our tool showing the states and probabilities of an HMM at first sight to the color concept of the website, the logo and the cylinders. Therefore, the insert states are blue and the deletion states gray.
\bigskip  
\bigskip 
\bigskip 
\bigskip 

\subsubsection{Settings}
To adjust the settings of the 3DView, several options had been included into tabs which can be shown or hidden by a mouse click (see figure \ref{fig:settings}).
\begin{center}
\includegraphics[scale=0.5]{settings.png}
\captionof{figure}{Overview of the steps performed by the web-server to create the website}
\label{fig:settings}
\end{center}
The following options with default values are implemented in the tool and shortly explained in this report.\\\\
\textbf{Amino acid settings:}
\begin{itemize}
\item Draw only the following number of most probable amino acids: 20 (0-20)
\item Draw only amino acids above this threshold (percentage): 0 (0-100)
\item Draw names of amino acids above this threshold (percentage): 8 (0-100)
\end{itemize}
After settings one of the above options, the complete scene is redrawn according to the new settings, this may take up to a minute depending on the HMM size and the computers hardware. It changes the amount of amino acids displayed in the AA bars using the specified thresholds/values.\\\\
\textbf{Insert/Delete-state options:}
\begin{itemize}
\item Draw actual probabilities (default)
\item Draw probability-differences
\end{itemize}
This option changes the display of the cylinders visualizing the delete and insert states. The default options shows the height of the bars as the actual probability information of the HMM to stay in the insert or delete state. If the option "draw probability-differences" is activated, this visualization changed to only show differences of the probabilities to stay in the insert or delete state of the HMM. Therefore changes can be detected very fast and the actual information can anytime still be shown by using the pop-up window. \\\\
\textbf{Colorings:}
\begin{itemize}
\item Single colors
\item Group colors
\end{itemize}
This option is used to switch through different coloring of the HMM. Details see section Coloring of this report.\\\\
\textbf{Additional features:}
\begin{itemize}
\item Reset camera
\end{itemize}
This button simply resets the camera to the default state.
\subsubsection{Amino acid table}
The probabilities of the amino acids and the states are extracted from the .hmm file and formatted to a HMM table in our tool. Because of the big data, the table is displayed larger than the website width and is therefore hidden by default. It can easily be shown by a single click if something has to be looked up in the actual HMM data.
\subsubsection{Bookmarking}
The bookmark function was implemented to simplify loading of specific and often used HMMs. If a single HMM is often used it is a long way to choose it each time in from the Home site and wait until it is loaded. Therefore the bookmark option can be used to get the information how exactly this page with the loaded HMM can be bookmarked for faster access later on.

\subsection{Index page}
The Index page is the entry site of our tool that can be reached via Home-Button or by click on the HMM Viewer Logo. It provides several functions to load .hmm-files and to navigate to the Upload-section and the Help-page. There are also links to bookmark a currently loaded HMM and to get information on the terms of use and the license used (About).\\
Referring to the color concept, the Header and Buttons as well as the Logo of HMM Viewer was chosen to be gray and blue trying to indicate a business-like and clean view that is comfortable to look at and does not distract the user from our tool.

\subsubsection{File-chooser}
The user can choose a previously uploaded .hmm file from a list of all available HMMs. In comparison to the import from Pfam (see chapter below) the 3DView is shown faster when choosing an uploaded file, as the server does not need to lookup and import the file before the visualization is shown. Another possible function implemented, is the deletion of uploaded HMMs from the list which has to be confirmed twice (button and message).

\subsubsection{Import from Pfam}
It is also possible to import a available HMM from Pfam by inserting the Pfam Accession number of a Family into the provided field for loading the information without the need of downloading the file from the Pfam website and uploading it via the Upload-function of our website.

\subsubsection{File upload}
This section can be reached via the Upload-Button of the menu. The user is asked in a dialogue to upload .hmm-files to be stored in the on the server. All uploaded files are later-on available in the file-chooser list.

\subsubsection{Help page}

Our Help-Button refers to a PDF-manual describing the functions of the tool. This PDF can also be reached by clicking the exclamation mark graphics. A short explanation tool-tip is also provided by hovering those graphics and the adequate section of the help file to the specific topic is displayed.

\section{Discussion}

\subsection{Settings}
After choosing one of the amino acid settings, the "update" buttons has to be pressed (see figure \ref{fig:settings}). This is due to the reason, that the complete visualization is redrawn after changing any of the given amino acid settings. To prevent a non-responsive user interface we decided to use a button instead of real-time updates for this specific option.

\subsection{Bookmarking}
For technical reasons it was not possible to directly bookmark the website on-click reliable on all operating systems and browsers. Therefore we decided to show a short manual how to bookmark with a hot-key.

\subsection{Save to file}
We currently have no option included to save the displayed HMM visualization as a picture (see discussion), but the website itself can be saved to the local disk by using the "save page" function of the browser. As the website visualization is done in JavaScript the website will display without problems from the local file.

\subsection{Evaluation - Comparing to other tools}
As one of our aims was to create a tool that visualizes also symbols with small probabilities very well, we found a good example for evaluating the concept of our tool to be 3D based. For example see figure \ref{fig:egf}, showing the visualization of HMMLogo for the EGF factor. The symbols with small probabilities, e. g. at positions 6 to 11 cannot be read.\\
\begin{center}
\includegraphics[scale=0.34]{EGF_CA_image.png}
\captionof{figure}{Visualization of HMMLogo for EGF HMM file.}
\label{fig:egf}
\end{center}
In contrast, the visualization in our tool, shown in figure \ref{fig:egf2}. The consensus sequence can be read easily.
\begin{center}
\includegraphics[scale=0.5]{egf.png}
\captionof{figure}{Visualization of HMM Viewer for EGF HMM file up to position 12.}
\label{fig:egf2}
\end{center}
Despite of viewing the HMM at the front, one can turn the camera for a top down view which makes reading the consensus sequence even easier (see figure \ref{fig:top}).
\begin{center}
\includegraphics[scale=0.5]{top.png}
\captionof{figure}{Visualization of HMM Viewer from MORN repeat in top view.}
\label{fig:top}
\end{center}
Additionally, we aimed for a better display of the transition probabilities. For this task we search for a HMM example that has different values. We found the HMM of the Sel1 repeat to emphasize the visualization of transition probabilities. Figure \ref{fig:sel} shows the visualization by HMMLogo representing the probabilities by the width of the lines between the states.\\
\begin{center}
\includegraphics[scale=0.35]{Sel1_image.png}
\captionof{figure}{Visualization of HMMLogo for Sel1 HMM file.}
\label{fig:sel}
\end{center}
The visualization in our tool seems to be much clearer than in HMMLogo also due to the function of displaying only probability differences (see figure \ref{fig:sel2}. So, the user can easily determine positions that are for special interest.
\begin{center}
\includegraphics[scale=0.5]{sel.png}
\captionof{figure}{Visualization of HMM Viewer for Sel1 HMM file, starting at position 7.}
\label{fig:sel2}
\end{center}
Finally, we can assume that our tool is a quite good alternative to visualize profile HMMs due to the aim of our project. As we did see, HMM Viewer is in favour of displaying probability differences if the user wants to find interesting positions in an HMM. The tool visualizes also the small probabilities of the symbols at each position and the consensus sequence can be read easily, also by turning the camera on the top for looking on the model from above. Nonetheless, there are some improvements to be work on in the future (see next section).

\subsection{Outlook}
As the time was limited during this project and it took quite an amount of time to get all the implemented features working, we unfortunately did not have enough time to implement all the features we wanted. Our main goal was achieved and we created a very solid tool to work with. However, over time we would like to see the following features and suggestions get implemented.

\subsubsection{Full-screen mode}
In addition to the 3D view now implemented in the website, we would like to have a full-screen mode, which will focus only on the 3D view and hide everything else. Currently the window has a fixed size and cannot be enlarged. For small monitors this is perfect as they have a small resolution. On bigger ones however, it would be nice to be able to enlarge the small window to the full-screen. This would also be a nice feature to make high-quality screen-shots.
\subsubsection{Save/restore camera}
An option to save the camera would also be a nice addition, because right now, if the windows is reloaded, the website resets the camera and if the user focused a specific position in the HMM it is now gone and the the user has to refocus on the site again. Therefore we suggest a feature to save the camera-position, for example in a hash, which can than be easily copied or even bookmarked, to get later-on back to exactly the view the user had when leaving the page.
\subsubsection{Save as picture}
For publications or for reports, which do not allow a interactive website to be published, a "save as picture" function would be a great addition. Right now it is absolutely no problem to simply focus on the site you want and take a screen-shot of the 3D view. However the "save as picture" option would be even more intuitive than making the screen-shot all by yourself.
\subsubsection{More color schemes}
At the moment we include only two color schemes. One for single amino acids and one to group them for chemical similarity. Other researchers however have developed even more color schemes. For example there is a nice latex package available which include a lot of different coloring schemes, called texshade \cite{texshade}. It would be nice to use all those different schemes in our tool, to group not only for chemical similarity but also for polarity or similar mass. 

\section{Supplementary}
\begin{itemize}
\item hmm-viewer.zip
\end{itemize}

\bibliographystyle{unsrt}
\bibliography{report}

\end{document}