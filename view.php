<!doctype html>
<!-- view.php

HMM Viewer, a new way of visualizing profile Hidden Markov Models.
Copyright (C) 2013 Jens Rauch and Bianca Regenbogen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses. -->
<html>
	<head>
		<meta charset="utf-8" />
		<title>HMM Viewer (Beta)</title>
		<!-- view.css -> layout hmm viewer, main_site.css -> layout website -->
		<link rel="stylesheet" href="./css/view.css">
		<link rel="stylesheet" href="./css/main_site.css">
		<script src="./js/Detector.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/three.min-r63.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/Stats.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/TrackballControls.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/jquery-2.0.3.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="./js/Hmmview.js" type="text/javascript" charset="UTF-8"></script>
		<link type="text/css" href="./css/custom-theme/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
		<script type="text/javascript" src="./js/jquery-ui-1.10.3.custom.min.js"></script>
	</head>
	<script>
		//use detector to show webgl message if webgl is disabled
		if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
		//initialize some needed variables for three.js
		var container, renderer, camera, scene, color, controls, projector;
		var WIDTH, HEIGHT;
		var firstround = true;
		function init(){
			// set basic config
			container = document.getElementById('container');
			WIDTH = 1000,
			HEIGHT = 600;  
			// camera and scene
			camera = new THREE.PerspectiveCamera( 45, WIDTH / HEIGHT, 1, 10000);
			camera.position.z = 60;
			camera.position.x = 50;
			camera.position.y = 5;
			scene = new THREE.Scene();
			scene.add( camera );
			// renderer and projector
			renderer = new THREE.WebGLRenderer( { antialias: true } );
			renderer.sortObjects = false;
			renderer.setSize(WIDTH, HEIGHT);
			projector = new THREE.Projector();
			container.appendChild(renderer.domElement);
			// controls
			controls = new THREE.TrackballControls( camera, container );
			targ = new THREE.Vector3( 50, 0, 0);
			controls.noZoom = true;
			controls.target = targ;
			// stats and alignment
			stats = new Stats();
			stats.getDomElement().style.position = 'absolute';
			stats.getDomElement().style.left = '0px';
			stats.getDomElement().style.top = '0px';
			document.body.appendChild( stats.getDomElement() );
			setInterval( function () { stats.update(); }, 1000 / 60 );
		}
		//animation and render functions for real-time interaction
		function animate(){
			requestAnimationFrame( animate );
			render();
			stats.update();
		}			
		function render(){
			controls.update();
			renderer.render(scene, camera);
		}
	</script>
	<body>
		<?php
		$ddir = "./hmms/";
		//set used file
			if(empty($_POST["id"])==FALSE or empty($_GET["id"])==FALSE){
				if(empty($_POST["id"])==FALSE){$filename = $_POST["id"];}
				if(empty($_GET["id"])==FALSE){$filename = $_GET["id"];}
				//check for valid ending, do not allow php files
				$tmp=explode(".",$filename);
				$isphp=array_pop($tmp);
				$phpending=array("php","php3","php4","php5");
				if(in_array($isphp, $phpending)){
					$result = "You have selected a .php file. This is due to security reasons not possible, please choose a valid HMM";
					echo '<script language="javascript" type="text/javascript">window.top.window.uploadProgress("<p>'.$result.'</p>");</script>';
					exit("PHP file upload is not allowed due to security reasons!");
				}
				unset($tmp);
				//remove any HTML anchors
				$pfam = explode("#",$filename);
				$filename = $pfam[0];
				//check for http protocol
				$pfam = explode(":",$filename);
				if($pfam[0] != "http") {
					$fpath = $ddir.$filename;
					if(file_exists($fpath)==TRUE){
						$file=fopen($fpath, "r") or exit("Unable to open file!");
					}
					else{ echo "<h1><span style='color: red;'><b>This is not a valid ID! Please visit the <a href='./index.php'>index.php</a></span></h1></b>"; }
				}
				else{
					$filename = $filename."/hmm";
					//check first if HMM file
					$firstline=`curl -s $filename | head -n1 | grep HMMER`;
					if($firstline != null) {
						$file=fopen($filename, "r") or exit("Unable to load HMM '$filename' from Pfam! Please check the URL");
					}
					else{
						echo "Unable to load HMM from Pfam! Please check the URL";
					}
				}
			}
			else{ echo "<h1><span style='color: red;'><b>You should not be here! Please visit the <a href='./index.php'>index.php</a></span></h1></b>"; }
				
				//open fixed file, debugging only
		//$file=fopen("./hmms/MORN.hmm", "r") or exit("Unable to open file!");
		if($file){
			//arrays for AS and AS sorting
			$hmm = array();
			$hmm["0"]=array("Position","A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y","m-m","m-i","m-d","i-m","i-i","d-m","d-d");
			$start=FALSE;
			$length=4;
			$i=1;
			?> 
			<div id="mainb">
				<div id="header">
					<span class="htext"><a href="./index.php" class="htext"><font color="#ffffff">HMM</font>Viewer</a></span><span class="htextadd">Beta</span>
					<div class="headnav">
						<a class="mentry" id="home" href="./index.php"><span id="ihome">Home</a></span>
						<a class="mentry" id="mupload"><span id="iupload">Upload</a></span>
						<a class="mentry" id="mhelp" href="./help.pdf" target="_blank"><span id="ihelp">Help</a></span>
					</div>
					<div>
						<br /><span class="bookmark">> <b>Bookmark</b> for direct access</span>
					</div>
				</div>
				<div id="mainsite">
					<?php include("./about.html"); ?>
					<div class="tooltip"></div>
					<div class="3dview">
						<div id="headw"><a href="./help.pdf#page=2" target="_blank"><div id="3dviewinfo" class="infopic"></div></a><div class="headtext">3D View</div></div>
							<div id="container">
								<div class="errormessage"><b><h2><span style="color: red">Please read this note: <br />If your browser is still loading the page (turning circle/loading bar) just wait a few minutes. If nothing happens, your WebGL is probably disabled. Most likely because "direct rendering" is disabled on your graphics card/browser. You have to enable it to use this site. You can get further informations in the <a href="../clans_web_viewer-help.pdf#page=14" target="_blank">FAQ of this web application</a> (see manual chapter 3.1)</span></h2></b></div>
							</div>
					</div>
					<div class="settings">
						<div id="headw"><div id="settings-hide" class="headtextmenu">hide tab</div><a href="./help.pdf#page=4" target="_blank"><div id="settingsinfo" class="infopic"></div></a><div class="headtext">Settings</div></div>
						<div class="hide">
							<h4>Amino acid settings:</h4>
							<input type="radio" name="aa" id="aanum" checked>Draw only the following number of most probable amino acids: <input type="number" name="aan" id="aanumn" min="1" max="20" class="maxas" value="20"><br />
							<div id="slider"></div><br />
							<input type="radio" name="aa" id="aaprob">Draw only amino acids above this threshold (percentage): <input type="number" name="aan" id="aaprobn" min="0" max="100" class="threshold" value="0"><br />
							<div id="sliderp"></div><br />
							Draw names of amino acids above this threshold (percentage): <input type="number" min="0" max="100" class="namethreshold" value="8"><br />
							<button class="menubutton" id="update">Update</button>
							<h4>Insert/Deletestate options:</h4>
							<input type="radio" name="prob" id="actual" checked /><label for="actual">Draw actual probabilities</label><br \>
							<input type="radio" name="prob" id="difference" /><label for="difference">Draw probability-differences</label><br \>
							<h4>Colorings:</h4>
							<input type="radio" name="color" id="colsingle" checked /><label for="colsingle">Single coloring</label><br \>
							<input type="radio" name="color" id="colgroup" /><label for="colgroup">Group coloring</label><br \>
							<h4>Additional features:</h4>
							<!-- <button class="camCons">Show consensus</button><br \> -->
							<p><button class="cam">Reset camera</button></p>
						</div>
					</div>
					<div class="atable">
						<div id="headw"><div id="atable-hide" class="headtextmenu">hide tab</div><a href="./help.pdf#page=5" target="_blank"><div id="aatableinfo" class="infopic"></div></a><div class="headtext">Amino Acid Table</div></div>
						<div class="hide">
							<table class="tab">
								<tr class="desc">
									<td>Position</td><td>A</td><td>C</td><td>D</td><td>E</td><td>F</td><td>G</td><td>H</td><td>I</td>
									<td>K</td><td>L</td><td>M</td><td>N</td><td>P</td><td>Q</td><td>R</td><td>S</td><td>T</td><td>V</td	><td>W</td><td>Y</td>
									<td>m-m</td><td>m-i</td><td>m-d</td><td>i-m</td><td>i-i</td><td>d-m</td><td>d-d</td>
								</tr> 
								<?php
								//read file, get values and fill table
								while(!feof($file)){
									$line=fgets($file);
									$line=preg_replace('#\s+#',' ',$line);
									$expline=explode(' ',$line);
									if(strcmp(trim($expline[0]),"LENG")==0){ $length=$expline[1]; }
									if($length>=$i){
										if(strcmp(trim($expline[1]),$i)==0){
											$start=TRUE;
											$hmm[$i] = array();
											echo "<tr>";
											for($i2=1;$i2<=21;$i2++){
											if($expline[$i2]!="*" && $expline[$i2]!="0" && $i!=$expline[$i2]){
													if($i%2==0){ echo "<td class='tabfield'>"; }
													else{ echo "<td class='second'>"; } 
													echo round(1/exp($expline[$i2]),5);
													$hmm[$i][$i2-1]=1/exp($expline[$i2]); 
												}
												elseif($i==$expline[$i2]){
													echo "<td class='desc'>";
													echo $expline[$i2];
													$hmm[$i][$i2-1]=$expline[$i2];
												}
												else{
													echo "<td>";
													echo $expline[$i2];
													$hmm[$i][$i2-1]=$expline[$i2];
												}
												echo "</td>";
											}
										}
										if($start==TRUE){
											$expline=explode(' ',$line);
											if(count($expline)<10){
												for($i2=1;$i2<=7;$i2++){
													if($i%2==0){echo "<td class='tabfield'>";}else{echo "<td class='second'>";}
													if($expline[$i2]!="*" && $expline[$i2]!="0"){
														echo round(1/exp($expline[$i2]),5);
														$hmm[$i][20+$i2]=1/exp($expline[$i2]);
													}
													else{
														echo $expline[$i2];
														$hmm[$i][20+$i2]=$expline[$i2];
													}
													echo "</td>";
												}
												echo "</tr>";
												$i++;
												$start=FALSE;
											}
										}
									}
								}?> 
							</table>
						</div>
					</div>
					
					<div id="dialog" title="Amino Acid Information">
						<p>List of AS:</p><p>Bar Number: </p><p>AS2</p><p>AS3</p>
					</div>
				<?php
				} ?>
			</div>
			<div id="footer">
				<span class="fooleft"><span class="fooright">> About</span>
			</div>
		</div>
		<script>
			$(document).ready(function(){
				//TODO move all stuff possible to seperate javascript file
				
				//get everything going
				init();
				animate();
				<?php print "hmm = ".json_encode($hmm).";"; ?>
				//function for dynamic creation of the axes lines based on HMM length
				hmm = new THREE.HmmViewer('random', hmm);
				axes = hmm.initAxes();
				scene.add(axes);
				hmm.createHmm(scene);
				//remove error if site/JS loaded successfully
				$('.errormessage').hide();
				//autohide tabs
				hideauto("settings-hide");
				hideauto("atable-hide");
				hideauto("coloring-hide");
				//eventhandler to show tabs on click
				hideshowm("settings-hide");
				hideshowm("atable-hide");
				hideshowm("coloring-hide");
				//add doubleclick event
				renderer.domElement.addEventListener( 'dblclick', hmm.DbCl, false );
				//setup slider
				$( "#slider" ).slider({
					min: 0,
					max: 20,
					value: 20,
					change: function(event, ui) {
					    $('.maxas').val(ui.value);
					}
				});
				$( "#sliderp" ).slider({
					min: 0,
					max: 100,
					value: 0,
					change: function(event, ui) {
					    $('.threshold').val(ui.value);
					}
				});
				//eventhandler for slider and update button
				$('input[name="aan"]').change(function(){
					if($(this).attr('id') == 'aanumn') {
						$("#slider").slider('value', $('.maxas').val());
					}
					else if($(this).attr('id') == 'aaprobn') {
						$("#sliderp").slider('value', $('.threshold').val());
					}
			    });
				$('#update').click(function(){
					if($('#aanum').is(':checked')) {
						maxas = $('.maxas').val();
						hmm.changeAs(maxas, scene);
					}
					else if($('#aaprob').is(':checked')) {
						thres = $('.threshold').val()/100;
						hmm.changeDrawAA(thres, scene);
					}
					nameThres = $('.namethreshold').val()/100;
					if(nameThres != hmm.drawAAName) {
						hmm.changeDrawAAName(nameThres, scene);
					}
				});
				//eventhandler for radiobuttons of AA drawing probability
				$('input:radio[name="prob"]').change(function(){
					if($(this).attr('id') == 'actual') {
						hmm.changeCalcState('act', scene);
					}
					else if($(this).attr('id') == 'difference') {
						hmm.changeCalcState('diff', scene);
					}
			    });
			    //eventhandler for radiobuttons of AA name drawing probability
				$('input:radio[name="color"]').change(function(){
					if($(this).attr('id') == 'colgroup') {
						hmm.setColor("group", scene);
					}
					else if($(this).attr('id') == 'colsingle') {
						hmm.setColor("single", scene);
					}
			    });
				//disable controls while hovering the menu
				$('#menu').hover(function(){
					controls.enabled=false;
				}, function(){
					controls.enabled=true;
				});
				$( "#dialog" ).dialog({ autoOpen: false, position: "right" });
				$( "#dialog" ).on( "dialogbeforeclose", function( event, ui ) {hmm.deselect(scene);} );				
				//show/hide tabs on website
				function hideauto(name){
					var $var = $('div#'+name);
						$var.text('show tab');
						$var.parent().parent().children('div.hide').hide();
				}
				function showauto(name){
					var $var = $('div#'+name);
						if($var.text()!='hide tab'){
							$var.text('hide tab');
							$var.parent().parent().children('div.hide').show();
						}
				}
				//bookmarkfunction
				$('.bookmark').on('click', function(){
					alert('Due to technical reasons please press ctrl+D to bookmark (Command+D for macs)');
				});
				//toggle visibility of tabs
				function hideshowm(name){
					var $var = $('div#'+name);
					$var.click(function(){
						if($var.text()=="show tab"){$var.text('hide tab');}
						else{$var.text('show tab');}
						if($var.parent().parent().children('div.hide').css('display') == "none"){
							$var.parent().parent().children('div.hide').show('slow');
						}
						else{ $var.parent().parent().children('div.hide').hide('slow'); }
					});
				}
				//create about popup
				$( "#adialog" ).dialog({ autoOpen: false, width: 600, position: "top" });
				//show about when clicking on the link
				$( "span.fooright").on('click', function(){
					$("#adialog").dialog('open');
				});
				//initialize Tooltip
				var tooltip = $('<div class="tooltip"></div>').text('');
				$(tooltip).appendTo('body');
				//show tooltips
				$('#settingsinfo').mouseenter(function(){
					tooltip.html('You can change various display options using this tab<br>See manual chapter 3.4')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
				$('#3dviewinfo').mouseenter(function(){
					tooltip.html('You can see the 3D view of the selected HMM below. Pan the 3D view with a left-click and rotate with a right-click.<br>See manual chapter 3.1')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
				$('#aatableinfo').mouseenter(function(){
					tooltip.html('A complete table of all AA probabilities can be displayed using this tab. <br><b>Warning</b>: This may be very large, depending on your HMM<br>See manual chapter 3.5')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
				//reset camera to default on click
				$('button.cam').on('click', function(){
					scene.remove( camera );
					delete camera;
					delete controls;					
					WIDTH = 1000,
					HEIGHT = 600;  
					// camera and scene
					camera = new THREE.PerspectiveCamera( 45, WIDTH / HEIGHT, 1, 10000);
					camera.position.z = 60;
					camera.position.x = 50;
					camera.position.y = 5;
					scene.add( camera );
					// controls
					controls = new THREE.TrackballControls( camera, container );
					targ = new THREE.Vector3( 50, 0, 0);
					controls.noZoom = true;
					controls.target = targ;
				});
				//switch to top down camera to focus on the most likely sequence
				$('button.camCons').on('click', function(){
					
				});
			});
		</script>
	</body>
</html>