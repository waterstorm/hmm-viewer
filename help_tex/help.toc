\select@language {english}
\contentsline {section}{\numberline {1}Instructions}{2}{section.0.1}
\contentsline {section}{\numberline {2}Index page}{2}{section.0.2}
\contentsline {subsection}{\numberline {2.1}File-chooser}{2}{subsection.0.2.1}
\contentsline {subsection}{\numberline {2.2}Import from Pfam}{2}{subsection.0.2.2}
\contentsline {subsubsection}{How it works}{2}{section*.2}
\contentsline {subsection}{\numberline {2.3}Upload page}{2}{subsection.0.2.3}
\contentsline {section}{\numberline {3}Display of data}{2}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Movement}{2}{subsection.0.3.1}
\contentsline {subsection}{\numberline {3.2}3D View}{2}{subsection.0.3.2}
\contentsline {subsection}{\numberline {3.3}Coloring schemes}{3}{subsection.0.3.3}
\contentsline {subsection}{\numberline {3.4}Settings}{4}{subsection.0.3.4}
\contentsline {subsubsection}{Amino acid settings:}{4}{section*.3}
\contentsline {subsubsection}{Insert/Deletestate options:}{4}{section*.4}
\contentsline {subsubsection}{Colorings:}{4}{section*.5}
\contentsline {subsubsection}{Additional features:}{4}{section*.6}
\contentsline {subsection}{\numberline {3.5}Amino acid table}{5}{subsection.0.3.5}
\contentsline {subsection}{\numberline {3.6}Bookmarking}{5}{subsection.0.3.6}
\contentsline {section}{\numberline {4}FAQ}{5}{section.0.4}
\contentsline {section}{\numberline {5}Supplementary}{6}{section.0.5}
\contentsline {subsection}{\numberline {5.1}About}{6}{subsection.0.5.1}
\contentsline {subsection}{\numberline {5.2}Terms of use}{6}{subsection.0.5.2}
