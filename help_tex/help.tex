\documentclass[10pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{float}
\restylefloat{table}
\usepackage{hyperref}
\usepackage{cite}
% Caption Settings:
\usepackage[hang]{caption}
\DeclareCaptionLabelFormat{bez}{#2}
\renewcommand{\thesection}{\arabic{section}}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{wrapfig}
\setcounter{tocdepth}{3}
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{Page \thepage\ of \pageref{LastPage}}
\begin{document}

\title{HMM Viewer - Manual}
\author{Bianca Regenbogen
        \and Jens Rauch}
        
\maketitle

\tableofcontents
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\begin{center}
HMM Viewer, a new way of visualizing profile Hidden Markov Models.\\
Copyright (C) 2013 Jens Rauch and Bianca Regenbogen\\

\end{center}
\newpage

\section{Instructions}
First of all it is necessary that your browser supports WebGl. Most of the major browsers
implemented this in the last few years. But there are great performance differences, therefore we recommend using Google Chrome (Version 9 or greater) or Firefox (Version 4 or greater).\\
However, it is better to use the current stable build of the browser of your choice, which was at the time writing this Firefox 26 and Chrome 32. If you experience any difficulties please consult the FAQ \ref{sec:faq}.

\section{Index page}
The Index page is the entry site of our tool that can be reached via Home-Button or by click on the HMM Viewer Logo. It provides several functions to load .hmm-files and to navigate to the Upload-section and the Help-page. It was designed to easily choose between the different possibilities for loading a .hmm file and visualize the HMM in a 3DView. Please note that depending on the file size and the processing power of your computer, the loading process of the 3DView may take some time.

\subsection{File-chooser}
You can choose a previously uploaded .hmm file from a list of all available HMMs. In comparison to the import from Pfam (see chapter below) the 3DView is shown faster when choosing an uploaded file, as the server does not need to lookup and import the file before the visualization is shown. Simply choose your file from the list and click on "choose for visualization".\\
Also available below the file-chooser list, is the deletion button.  It can be used to delete uploaded HMMs from the list which has to be confirmed twice (button and message) to prevent unintended deletions.

\subsection{Import from Pfam}
It is also possible to import a available HMM from Pfam by inserting the Pfam Accession number of a Family into the provided field for loading the information without the need of downloading the file from the Pfam website and uploading it via the Upload-function of our website.
\subsubsection{How it works}
\begin{itemize}
	\item Visit the Pfam website: http://pfam.sanger.ac.uk/browse
	\item Choose any family you want to view in our tool, for example \textit{GAF}
	\item Copy the URL from the browser, in this case: http://pfam.sanger.ac.uk/family/PF01590
	\item Paste it into the text-field on the index page of our tool
\end{itemize}

\subsection{Upload page}
This section can be reached via the Upload-Button of the menu. You are asked in a dialogue to upload .hmm-files to be stored in the on the server. Simply select any HMM file on your hard-disk and it will be uploaded and is later-on available in the file-chooser list on the index page.

\section{Display of data}
\subsection{Movement}
The default movement for the HMM Viewer is panning the camera. This can be achieved by dragging while pressing down the left mouse button. As an extended feature you can also rotate the camera with the use of the right mouse button. You can reset the camera at any time using the "reset camera" button in the settings tab. It is currently not possible to zoom in or out. 
\subsection{3D View}
Each bar in the 3D view is subdivided in smaller cubes, each one representing one specific amino acid. The short name of each AA is printed directly on the cube (if the cube is big enough) and each amino acid has a specific color (see section coloring). The height of these bars represents the probability of the HMM staying in the match state.\\
\begin{center}
\includegraphics[scale=0.65]{instructions.png}
\end{center}
The cylinders below the bars represent the insert- (blue) and delete states (gray). The thickness of the cylinders gets smaller, representing the probability of changing from a match state to an insert/delete state. For example, if the blue cylinder does not get smaller, the HMM has a probability of 100\% to change to the insert state at this position, while in contrary if it looks like a cone, the HMM has a probability of 0\% to change to the insert state.\\
The height of the cylinders shows the probability to stay in the actual state (analogue to the bars of the match state).\\
These detailed information are also presented in a detail pop-up, which can be displayed by a simple double-click on a specific position.


\subsection{Coloring schemes}
For displaying the different amino acids contained in the HMM, two color schemes were created: One for coloring each amino acid by a single color and another one using one color for chemical similarities (special cases: C, G and P) of amino acids.\\
The group colors were chosen similar to the colors used by ClustalX, JalView and Pfam (Table 1). \\
For the single colors, variants of the group color had been chosen which also indicate the chemical characteristics (Table 2).\\
\begin{table}[h]
\centering
\begin{tabular}{llc}
AAs & group & color \\  
\hline
WLVIMFA & hydrophobic & \colorbox[HTML]{377EB8}{ } \\ 
KR & positively charged & \colorbox[HTML]{E41A1C}{ } \\ 
TSNQ & polar uncharged & \colorbox[HTML]{4DAA4A}{ } \\ 
DE & negatively charged & \colorbox[HTML]{984EA3}{ } \\ 
HY & aromatic & \colorbox[HTML]{A65628}{ } \\ 
C & Cysteine & \colorbox[HTML]{F781BF}{ } \\ 
G & Glycine & \colorbox[HTML]{FF7F00}{ } \\ 
P & Proline & \colorbox[HTML]{FFFF33}{ } \\ 
\hline
\end{tabular} 
 \caption{Group coloring}
\end{table}

\begin{table}[h]
\centering
\begin{small}
\begin{tabular}{cc}
AA & color \\ 
\hline
W & \colorbox[HTML]{C6DBEF}{ } \\ 
L & \colorbox[HTML]{9ECAE1}{ } \\
V & \colorbox[HTML]{6BAED6}{ } \\ 
I & \colorbox[HTML]{4292C6}{ } \\ 
M & \colorbox[HTML]{2171B5}{ } \\ 
F & \colorbox[HTML]{08519C}{ } \\ 
A & \colorbox[HTML]{08306B}{ } \\ 
K & \colorbox[HTML]{FB6A4A}{ } \\ 
R & \colorbox[HTML]{CB181D}{ } \\ 
T & \colorbox[HTML]{A1D99B}{ } \\ 
S & \colorbox[HTML]{74C476}{ } \\ 
N & \colorbox[HTML]{31A354}{ } \\ 
Q & \colorbox[HTML]{006D2C}{ } \\ 
D & \colorbox[HTML]{C51B8A}{ } \\ 
E & \colorbox[HTML]{7A0177}{ } \\ 
H & \colorbox[HTML]{CC4C02}{ } \\ 
Y & \colorbox[HTML]{A65628}{ } \\ 
C & \colorbox[HTML]{F781BF}{ } \\ 
G & \colorbox[HTML]{FF7F00}{ } \\ 
P & \colorbox[HTML]{FFFF33}{ } \\ 
\hline
\end{tabular}
\caption{Single coloring} 
\end{small}
\end{table}
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip

\subsection{Settings}
To adjust the settings of the 3DView, several options had been included into tabs which can be shown or hidden by a mouse click.
\subsubsection{Amino acid settings:}
\begin{itemize}
\item Draw only the following number of most probable amino acids: 20 (0-20)
\item Draw only amino acids above this threshold (percentage): 0 (0-100)
\item Draw names of amino acids above this threshold (percentage): 8 (0-100)
\end{itemize}
After settings one of the above options, the complete scene is redrawn according to the new settings, this may take up to a minute depending on the HMM size and the computers hardware. It changes the amount of amino acids displayed in the AA bars using the specified thresholds/values.
\subsubsection{Insert/Deletestate options:}
\begin{itemize}
\item Draw actual probabilities (default)
\item Draw probability-differences
\end{itemize}
This option changes the display of the cylinders visualizing the delete and insert states. The default options shows the height of the bars as the actual probability information of the HMM to stay in the insert or delete state. If the option "draw probability-differences" is activated, this visualization changed to only show differences of the probabilities to stay in the insert or delete state of the HMM. Therefore changes can be detected very fast and the actual information can anytime still be shown by using the pop-up window.
\subsubsection{Colorings:}
\begin{itemize}
\item Single colors
\item Group colors
\end{itemize}
This option is used to switch through different coloring of the HMM. Details see section Coloring of this report.
\subsubsection{Additional features:}
\begin{itemize}
\item Reset camera
\end{itemize}
This button simply resets the camera to the default state.
\subsection{Amino acid table}
The probabilities of the amino acids and the states are extracted from the .hmm file and formated to a HMM table in our tool. Because of the big data, the table is displayed larger than the website width and is therefore hidden by default. It can easily be shown by a single click if something has to be looked up in the actual HMM data.
\subsection{Bookmarking}
The bookmark function was implemented to simplify loading of specific and often used HMMs. If a single HMM is often used it is a long way to choose it each time in from the Home site and wait until it is loaded. Therefore the bookmark option can be used to get the information how exactly this page with the loaded HMM can be bookmarked for faster access later on.

\section{FAQ}
\label{sec:faq}
\subsection*{Q1: I get this message: Your browser or your graphics card does not seem to support WebGL. What to do now?}
A: There are different cases (ordered by likelihood):\\
1. In most of the cases it will be related to your browser. You should check if your browser really supports WebGL. We recommend using Google Chrome ( https://www.google.com/chrome ), because it has, according to our tests, the best WebGL performance. But Firefox (
http://www.mozilla.org/en-US/products/download.html ) does also do a nice job. 
A list of possible browsers can be found here: http://en.wikipedia.org/wiki/Webgl.\\
Note: It has to be at least Chrome version 9 or Firefox version 4.\\
2. If you are using Firefox 4 or grater and this still occurs please refer to Q4.\\
3. If you are using Linux and a compatible browser please refer to Q3 \\
4. If you are using a very old computer it may be the case that your graphics card is too old. For a list of blacklisted cards please check this website:
http://www.khronos.org/webgl/wiki/BlacklistsAndWhitelists
(But you can still try out your card, because these things change really fast)
\subsection*{Q2: The performance is painfully bad what can I do to improve the it?}
A: It depends on a bunch of things.\\
1. The first thing you should try out is using Google Chrome (if you not already do). The
performance, at least under Linux, is way better than with any other browser.\\
2. If that does not help try to update your graphics card driver a collection of links can be found
here:\\
- AMD/ATI: http://support.amd.com/US/Pages/AMDSupportHub.aspx\\
- nVidia: http://www.nvidia.de/Download/index.aspx?lang=de\\
- Intel: http://downloadcenter.intel.com/Default.aspx?lang=deu\&changeLang=true\\
3. It could also be the case that your graphics card is too slow. But  this should only apply to really old graphics cards.
\subsection*{Q3: I am using Linux and I have downloaded one of the browsers above, but it is still not working. What now?}
A: You should check if you have installed the correct driver for your graphics card and if direct
rendering is enabled. The easiest way of doing so is opening a terminal and typing:\\
\texttt{glxinfo | grep "direct rendering"}\\
This should show:
\texttt{direct rendering: Yes/No}\\
Note: You need the "mesa-utils" oder "mesa-progs" installed for this command.\\
For ubuntu type:\\
\texttt{apt-get install mesa-utils}\\
If you have a nvidia graphics card and have the proprietary driver installed you can simply go to the
nvidia-settings and check the "OpenGL/GLX Information" tab. The first line will say "Direct Rendering: yes/no"\\
If direct rendering is not enabled you have to enable it to use this application. To do so please check
the manual of your Linux Distribution. For ubuntu you would probably like to check this site:\\
http://ubuntuforums.org/tags.php?tag=direct+rendering.
\subsection*{Q4: I want to use Firefox and I have already the newest version installed on my computer, but it still shows me some weird messages like: "If you can read this message longer than a few seconds your WebGL is disabled" or like the one above "Your browser or your graphics card does not seem to support WebGL."}
A: As mentioned before, Firefox has disabled WebGL on some computers. It is probably related to the
graphics card. But you can anyway force Firefox to enable WebGL so you can use the
web-application. To do so just follow this guide:\\
- Open a new browser tab in Firefox\\
- Type in the address-bar: about:config\\
- Click on the I’ll be careful I Promise button to continue\\
- Type webgl in the search field\\
- Double-click (enable) webgl.force-enabled so it says Value: true
\subsection*{Q5: I have opened a big file, it took a long time to load and sometimes the website responds slowly. Why is that?}
A: Your browser has to handle a very large set of data. At the moment WebGL uses your CPU and
your graphics card heavily. Depending on your system configuration it just takes time to display or
hide this large amount of data. Please be patient in such a case.
\section{Supplementary}
\subsection{About}
This Web-application was developed by Jens Rauch and Bianca Regenbogen at the University of Tuebingen as a project for the lecture Bioinformatics I.\\
Feel free to report bugs by contacting us or by visiting the GIT repository located at Bitbucket.org:\\ https://bitbucket.org/waterstorm/hmm-viewer/ \\
Contact Jens Rauch: Jens.Rauch[at]uni-konstanz.de\\
Contact Bianca Regenbogen: Bianca.Regenbogen[at]student.uni-tuebingen.de

\subsection{Terms of use}	
This program is free software: you can redistribute it and/or modify 	it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\\\\
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.\\\\	
You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses.


\end{document}