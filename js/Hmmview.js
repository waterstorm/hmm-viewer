// Hmmview.js
// 
//HMM Viewer, a new way of visualizing profile Hidden Markov Models.
//Copyright (C) 2013 Jens Rauch and Bianca Regenbogen
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.

/**
 * @author Jens Rauch
 * @author Bianca Regenbogen
 */

THREE.HmmViewer = function (colormap, hmm) {

	// init needed vars for AS name, coloring and the complete HMM from JSON
	var _this = this;
	this.color = colormap;
	//define one letter code & color for AS 
	this.as = ["A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y"];
	this.asnum = 20;
	// actual color, init as single color
	this.ascol = ["0x08306b","0xf781bf","0xc51b8a","0x7a0177","0x08519c","0xff7f00","0xcc4c02","0x4292c6","0xfb6a4a","0x9ecae1","0x2171b5","0x31a354","0xffff33","0x006d2c","0xcb181d","0x74c476","0xa1d99b","0x6baed6","0xc6dbef","0xa65628"];
	// AS colored by chemical groups
	this.colGroup = ["0x377eb8","0xf781bf","0x984ea3","0x984ea3","0x377eb8","0xff7f00","0xa65628","0x377eb8","0xe41a1c","0x377eb8","0x377eb8","0x4daf4a","0xffff33","0x4daf4a","0xe41a1c","0x4daf4a","0x4daf4a","0x377eb8","0x377eb8","0xa65628"];
	// single color for each AS
	this.colSingle = ["0x08306b","0xf781bf","0xc51b8a","0x7a0177","0x08519c","0xff7f00","0xcc4c02","0x4292c6","0xfb6a4a","0x9ecae1","0x2171b5","0x31a354","0xffff33","0x006d2c","0xcb181d","0x74c476","0xa1d99b","0x6baed6","0xc6dbef","0xa65628"];
	this.hmm = hmm;
	// set colors for inserts,deletions,axes and background
	this.colInsert = "0x3b679e";
	this.colDeletion = "0x999999";
	this.colAxes = "0x000000";
	this.colBackground = "0xDDDDDD";
	this.colLines = "0x000000";
	// draw AA threshold
	this.drawAA = 0;
	// draw AA name threshold
	this.drawAAName = 0.08;
	// set insert/deletion state calculation method (legit values: act, diff)
	this.calcState = 'act';
	// set scaling factor for interval between two matches
	this.intFac = 6;
	this.xOffset = 3;
	// init some internal stuff
	this.mouse = new THREE.Vector2();
	this.objects = [];
	this.last = 0;
	
	// initialize axes based on length/width of the HMM
	this.initAxes = function() {
		group = new THREE.Object3D();
		col = parseInt(_this.colAxes.replace(/"/g, ""), 16);
		var lineGeo = new THREE.Geometry();
		// push lines, until loop reaches the end of the HMM
		lineGeo.vertices.push(
			vec(0, 0, 0), vec((hmm.length-1)*_this.intFac+5, 0, 0),
			vec(0, 0, 0), vec(0, 22, 0)
		);
		for(i=0;i<=hmm.length-1;i++) {
			lineGeo.vertices.push( vec(i*_this.intFac, 0, 0), vec(i*_this.intFac, 0, 0) );
		}
		// set color and material
		var lineMat = new THREE.LineBasicMaterial( {color: col, lineWidth: 1} );
		var line = new THREE.Line(lineGeo, lineMat);
		line.type = THREE.Lines;
		// add to and return group
		group.add(line);
		return group;
	};
	
	// create actual HMM vis by creating match, insert and delete states.
	// maxas can be used to restrict the vis to a certain amount of AAs
	this.createHmm = function(scene) {
		maxas = this.asnum;
		backCol = parseInt(_this.colBackground.replace(/"/g, ""), 16);
		// add subtle ambient lighting
        var ambientLight = new THREE.AmbientLight(0x383838);
        scene.add(ambientLight);
        renderer.setClearColor(backCol,0);
        // add directional light source
        var directionalLight = new THREE.DirectionalLight(0xffffff);
        directionalLight.position.set(1, 2, 3).normalize();
        scene.add(directionalLight);
		//scaling factor for probabilites
		scaling=20;
		depScaling=3.5;
		//parse colors
		insCol = parseInt(_this.colInsert.replace(/"/g, ""), 16);
		delCol = parseInt(_this.colDeletion.replace(/"/g, ""), 16);
		//reset objects
		this.objects = [];
		//create matches
		for(i=0;i<=hmm.length-2;i++) {
			line = getLine(hmm, i, true, true);
			absheight=0;
			bar = new Array();
			matchhit = new Array();
			bar[i] = new THREE.Object3D();
			for(i2=20-maxas;i2<20;i2++) {
				if(line[i2][1] < _this.drawAA) {
					continue;
				}
				if(line[i2][1] > _this.drawAAName) {
					match = new THREE.Mesh( new THREE.CubeGeometry(4.5,line[i2][1]*scaling,hmm[i+1][21]*depScaling), createLabel(line[i2][0], 20, "black", "#"+line[i2][2].toString().substr(2)) );
				}
				else {
					match = new THREE.Mesh( new THREE.CubeGeometry(4.5,line[i2][1]*scaling,hmm[i+1][21]*depScaling), new THREE.MeshLambertMaterial({ color: "#"+line[i2][2].toString().substr(2) }) );
				}
				match.position.x = i*_this.intFac+_this.xOffset;
				match.position.y = absheight+line[i2][1]*scaling/2;
				match.position.z = -hmm[i+1][21]*depScaling/2;
				bar[i].add( match );
				bar[i].scale.y = hmm[i+1][21];
				absheight=absheight+line[i2][1]*scaling;
			}
			scene.add( bar[i] );
			matchhit[i] = new THREE.Mesh( new THREE.CubeGeometry(5,20,3), new THREE.MeshBasicMaterial({ color: 0x000000, wireframe: true }) );
			matchhit[i].position.x = i*_this.intFac+_this.xOffset;
			matchhit[i].position.y = 10;
			matchhit[i].visible = false;
			scene.add( matchhit[i] );
			_this.objects.push( matchhit[i] );
		}
		//create inserts
		ins = new Array();
		for(i=0;i<=hmm.length-3;i++) {
			if(_this.calcState=="diff") {
				if(i != hmm.length-3){
					insheight = Math.abs((hmm[i+1][25]*scaling)-(hmm[i+2][25]*scaling));
				}
				else {
					insheight = 0;
				}
			}
			else {
				insheight = hmm[i+1][25]*scaling;
			}
			ins[i] = new THREE.Mesh( new THREE.CylinderGeometry(1,parseFloat(roundn(hmm[i+1][22],5)),insheight,4), new THREE.MeshLambertMaterial({ color: insCol }) ); 
			ins[i].position.x = i*_this.intFac+4.5;
			ins[i].position.y = -insheight/2;
			// ins[i].position.z = -16;
			// ins[i].rotation.y = 0.8;
			scene.add( ins[i] );
		}
		//create deletions
		del = new Array();
		for(i=0;i<=hmm.length-2;i++) {
			if(_this.calcState=="diff") {
				if(i != hmm.length-2){
					insheight = Math.abs((hmm[i+1][27]*scaling)-(hmm[i+2][27]*scaling));
				}
				else {
					insheight = 0;
				}
			}
			else {
				insheight = hmm[i+1][27]*scaling;
			}
			del[i] = new THREE.Mesh( new THREE.CylinderGeometry(1,parseFloat(roundn(hmm[i+1][23],5)),insheight,10), new THREE.MeshLambertMaterial({ color: delCol }) ); 
			del[i].position.x = i*_this.intFac+1.5;
			del[i].position.y = -insheight/2;;
			// del[i].position.z = -8;
			scene.add( del[i] );
		}
		this.last = i-1;
	};
	
	// function to redraw HMM with net settings
	redraw = function(scene) {
		//close dialog to prevent bugs with the highlighting and lines
		$( "#dialog" ).dialog('close');
		//reset scene
		var obj, i;
		for ( i = scene.children.length - 1; i >= 0 ; i -- ) {
		    obj = scene.children[ i ];
		    if ( obj !== camera ) {
		        scene.remove(obj);
		    }
		}
		//reinit axes
		scene.add(_this.initAxes());
		//redraw
		_this.createHmm(scene, _this.asnum);
	};
		
	// set a coloring sheme for AS
	this.setColor = function(colormap, scene) {
		if(colormap == "group") {
			this.ascol = this.colGroup;
		}
		else if(colormap == "single") {
			this.ascol = this.colSingle;
		}
		redraw(scene);
	};
	
	// change the color of the insert bars on demand
	this.setInserColor = function(color, scene) {
		this.colInsert = color;
		redraw(scene);
	};
	
	// change the color of the deletion bars on demand
	this.setDeletionColor = function(color, scene) {
		this.colDeletion = color;
		redraw(scene);
	};
	
	//change number of AS to draw
	this.changeAs = function(maxas, scene) {
		this.asnum = maxas;
		this.drawAA = 0;
		redraw(scene);
	};
	
	//change background color
	this.changeBackground = function(color) {
		this.colBackground = color;
		backCol = parseInt(_this.colBackground.replace(/"/g, ""), 16);
        renderer.setClearColor(backCol,0);
	};
	
	//change AA name drawing
	this.changeDrawAAName = function(prob, scene) {
		this.drawAAName = prob;
		redraw(scene);
	};
	
	//change AA probability drawing
	this.changeDrawAA = function(prob, scene) {
		this.drawAA = prob;
		this.asnum = 20;
		redraw(scene);
	};
	
	//change calculation of insert/deletestates
	this.changeCalcState = function(state, scene) {
		if(state == 'diff') {
			this.calcState = 'diff';
		}
		else {
			this.calcState = 'act';
		}
		redraw(scene);
	};
	
	// returns AS name
	getas = function(a){
		asfull = ["Alanin","Cystein","Aspartic acid","Glutamic acid","Phenylalanine","Glycine","Histidine","Isoleucine","Lysine","Leucine","Methionine","Asparagine","Proline","Glutamine","Arginine","Serine","Threonine","Valine","Tryptophan","Tyrosine"];
		return asfull[_this.as.indexOf(a)];
	};
	
	// round with n positions after decimal point
	roundn = function(x, n){
		if (n < 1 || n > 14) return false;
		var e = Math.pow(10, n);
		var k = (Math.round(x * e) / e).toString();
		if (k.indexOf('.') == -1) k += '.';
		k += e.toString().substring(1);
		return k.substring(0, k.indexOf('.') + n+1);
	};
	
	// return axes lines
	vec = function(x,y,z){ 
	    return new THREE.Vector3(x,y,z); 
	};
	
	// sort AS according to probability
	function mySorting(a,b) {
		a = a[1];
		b = b[1];
		return a == b ? 0 : (a > b ? -1 : 1);
	};
	function mySortingReverse(a,b) {
		a = a[1];
		b = b[1];
		return a == b ? 0 : (a < b ? -1 : 1);
	};
	
	// get current line and sort it by probability
	function getLine(hmm, lineNr, hex, reverse) {
		line = [];
		for(t=0;t<20;t++){
			//convert color from hex to decimal
			if(!hex) {
				col = parseInt(_this.ascol[t].replace(/"/g, ""), 16);
			}
			else {
				col = _this.ascol[t];
			}
			line[t]=[_this.as[t],hmm[lineNr+1][t+1],col];
		}
		if(reverse) {
			line=line.sort(mySortingReverse);
		}
		else {
			line=line.sort(mySorting);
		}
		return line;
	}
	
	createLabel = function(text, size, color, backGroundColor, backgroundMargin) {
		if(!backgroundMargin)
			backgroundMargin = 50;
		 
		var canvas = document.createElement("canvas");
		 
		var context = canvas.getContext("2d");
		context.font = size + "pt Arial";
		 
		var textWidth = context.measureText(text).width;
		 
		canvas.width = textWidth + backgroundMargin;
		canvas.height = size + backgroundMargin;
		context = canvas.getContext("2d");
		context.font = size + "pt Arial";
		 
		if(backGroundColor) {
			context.fillStyle = backGroundColor;
			context.fillRect(canvas.width / 2 - textWidth / 2 - backgroundMargin / 2, canvas.height / 2 - size / 2 - +backgroundMargin / 2, textWidth + backgroundMargin, size + backgroundMargin);
		}
		 
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.fillStyle = color;
		context.fillText(text, canvas.width / 2, canvas.height / 2);
		 
		// context.strokeStyle = "black";
		// context.strokeRect(0, 0, canvas.width, canvas.height);
		 
		var texture = new THREE.Texture(canvas);
		texture.needsUpdate = true;
		 
		var material = new THREE.MeshLambertMaterial({
			map : texture
		});
		 
		return material;
	};
	
	// get current probabilities
	function getProb(hmm, lineNr) {
		line = new Array();
		for(t=20;t<27;t++){
			line[t]=hmm[lineNr+1][t+1];
		}
		return line;
	}

	this.deselect = function(scene) {
		// scene.remove( connections );
		scene.remove( cube );
	};
	
	// get pos relative to container
	getMouseInContainer = function(clientX, clientY) {
		screen = { width: window.innerWidth, height: window.innerHeight, offsetLeft: 0, offsetTop: 0 };
		if (!container) {
			return new THREE.Vector2(
				(clientX - screen.width * 0.5 - screen.offsetLeft) / (screen.width * 0.5),
				(screen.height * 0.5 + screen.offsetTop - clientY) / (screen.height * 0.5));
		}
		var currentElement = container;
		var totalOffsetX = currentElement.offsetLeft - currentElement.scrollLeft;
		var totalOffsetY = currentElement.offsetTop - currentElement.scrollTop;
		var containerY   = 0;
		var containerX	 = 0;
		var canvasY      = 0;
		
		while (currentElement = currentElement.offsetParent) {
			totalOffsetX += currentElement.offsetLeft - $(document).scrollLeft();
			totalOffsetY += currentElement.offsetTop - $(document).scrollTop();
		}
		
		containerX = clientX - totalOffsetX;
		containerY = clientY - totalOffsetY;
		return new THREE.Vector2(
			(containerX - container.offsetWidth * 0.5) / (container.offsetWidth * .5),
			(container.offsetHeight * 0.5 - containerY) / (container.offsetHeight * .5));
	};
	
	// double-click event
	this.DbCl = function( event ) {
		//get mouse position
		event.preventDefault();
		_this.mouse = getMouseInContainer(event.clientX, event.clientY);
		var vector = new THREE.Vector3( _this.mouse.x, _this.mouse.y, 0.5 );
		projector.unprojectVector( vector, camera );
		//check for intersection
		var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );
		var intersects = ray.intersectObjects( _this.objects );
		if ( intersects.length > 0 ) {
			//remove old connections (if available)
			if(firstround==false){
				$("#dialog").dialog('close');
				// scene.remove( connections );
				scene.remove( cube );
			}
			//get id of intersected object
			nr=(intersects[ 0 ].object.position.x-_this.xOffset)/_this.intFac;
			//highlight match
			cube = new THREE.BoxHelper( intersects[ 0 ].object );
			cube.material.color.set( 0x88dd11 );
			scene.add( cube );
			//get sorted list of probabilities
			listsort = getLine(hmm, nr, true, false);
			probl = getProb(hmm, nr);
			// construct popup in HTML
			list="Amino Acid Information:<br>";
			list+="------------------------";
			list+="<table><tr><td>Color</td><td>AA</td><td>Probability</td><td>%</td><td>Full name</td></tr>";
			for(i=0;i<20;i++){
				list=list+"<tr><td><div id='square' style='background-color: #"+listsort[i][2].substr(2)+"; border:1px solid #"+listsort[i][2].substr(2)+"'></div></td><td>"+listsort[i][0]+"</td><td>"+roundn(listsort[i][1],5)+"</td><td>"+roundn(listsort[i][1]*100,2)+"</td><td>"+getas(listsort[i][0])+"</td></tr>";
			}
			list+="</table>";
			list+="------------------------";
			list+="<table><tr><td>Icon</td><td>From</td><td></td><td>to</td><td>Probability</td><td>%</td></tr>";
			list+="<tr><td></td><td>Match</td><td>-</td><td>Match</td><td>"+roundn(probl[20],5)+"</td><td>"+roundn(probl[20]*100,2)+"</td></tr>";
			list+="<tr><td></td><td>Match</td><td>-</td><td>Insert</td><td>"+roundn(probl[21],5)+"</td><td>"+roundn(probl[21]*100,2)+"</td></tr>";
			list+="<tr><td></div></td><td>Match</td><td>-</td><td>Deletion</td><td>"+roundn(probl[22],5)+"</td><td>"+roundn(probl[22]*100,2)+"</td></tr>";
			list+="<tr><td></td><td>Insert</td><td>-</td><td>Match</td><td>"+roundn(probl[23],5)+"</td><td>"+roundn(probl[23]*100,2)+"</td></tr>";
			list+="<tr><td><div id='rotatedsquare' style='background-color: #"+_this.colInsert.substr(2)+"; border:1px solid #"+_this.colInsert.substr(2)+"'></div></td><td>Insert</td><td>-</td><td>Insert</td><td>"+roundn(probl[24],5)+"</td><td>"+roundn(probl[24]*100,2)+"</td></tr>";
			list+="<tr><td></td><td>Deletion</td><td>-</td><td>Insert</td><td>"+roundn(probl[25],5)+"</td><td>"+roundn(probl[25]*100,2)+"</td></tr>";
			list+="<tr><td><div id='circle' style='background-color: #"+_this.colDeletion.substr(2)+"; border:1px solid #"+_this.colDeletion.substr(2)+"'></div></td><td>Deletion</td><td>-</td><td>Deletion</td><td>"+roundn(probl[26],5)+"</td><td>"+roundn(probl[26]*100,2)+"</td></tr></table>";
			tit="Position: "+(nr+1);
			$("#dialog").html(list);
			// create new connection lines
			// connections = new THREE.Object3D();
			// var lineGeo0 = new THREE.Geometry(), lineGeo1 = new THREE.Geometry(), lineGeo2 = new THREE.Geometry();
			// if(nr!=_this.last){
				// probln = getProb(hmm, nr+1);
				// posd = new THREE.Vector3;
				// posd = del[nr].position;
				// heightd = posd.y*2;
				// posi = new THREE.Vector3;
				// posi = ins[nr].position;
				// heighti = posi.y*2;
				// lineGeo0.vertices.push(
					// vec(intersects[ 0 ].object.position.x, 0, intersects[ 0 ].object.position.z), vec(posd.x, heightd, posd.z)	
				// );
				// lineGeo1.vertices.push(
					// vec(intersects[ 0 ].object.position.x, 0, intersects[ 0 ].object.position.z), vec(posi.x, heighti, posi.z)
				// );
				// lineGeo2.vertices.push(
					// vec(intersects[ 0 ].object.position.x, intersects[ 0 ].object.position.y*2*probl[20], intersects[ 0 ].object.position.z), vec(intersects[ 0 ].object.position.x+_this.intFac, intersects[ 0 ].object.position.y*2*probln[20], intersects[ 0 ].object.position.z)
				// );
			// }
			// set thickness of line
			// Note: the thickness of the lines may not work in windows due to a bug in ANGLE
			// linesCol = parseInt(_this.colLines.replace(/"/g, ""), 16);
			// thick = probl[23]*20;
			// con = new THREE.Line(lineGeo0, new THREE.LineBasicMaterial({ color: linesCol, linewidth: thick }));
			// connections.add(con);
			// thick = probl[22]*20;
			// con = new THREE.Line(lineGeo1, new THREE.LineBasicMaterial({ color: linesCol, linewidth: thick }));
			// connections.add(con);
			// thick = probl[21]*20;
			// con = new THREE.Line(lineGeo2, new THREE.LineBasicMaterial({ color: linesCol, linewidth: thick }));
			// connections.add(con);
			// scene.add(connections);
			firstround=false;
			// show pop-up and center camera on specific position
			$("#dialog").dialog('open');
			$("#dialog").dialog( "option", "title", tit );
			delete camera;
			delete controls;
			camera = new THREE.PerspectiveCamera( 45, WIDTH / HEIGHT, 1, 10000);
			scene.add( camera );
			camera.position.z = 60;
			camera.position.x = intersects[ 0 ].object.position.x;
			camera.position.y = 5;
			controls = new THREE.TrackballControls( camera, container );
			targ.x = intersects[ 0 ].object.position.x;
			targ.y = 0;
			targ.z = 0;
			controls.target = targ;
			controls.noZoom = true;
		}
	};
};